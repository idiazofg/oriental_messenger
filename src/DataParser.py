import csv
import requests
import json
from datetime import datetime
from time import sleep

# csv_file = open('D:/home/site/wwwroot/src/api_data/data_csv.csv')
csv_file = open('./api_data/test_file.csv')
read_csv = csv.reader(csv_file, delimiter=",")
next(read_csv)
next(read_csv)
next(read_csv)

# report_file = open('D:/home/site/wwwroot/src/api_data/report.csv', mode='w')
report_file = open('./api_data/report.csv', mode='w')
fieldnames = ['Loan Number', 'Phone Number', 'Status', 'Description', 'Date Sent']
writer = csv.DictWriter(report_file, fieldnames=fieldnames)
writer.writeheader()

# Make request to authenticate to API and get Session ID

# Session Keys
landline_key = "7eff63c6-cdcf-4907-afc1-40ab9b0aadec:388640903"
# numverify_key = "c4d5ca35662b56ae6071d703a186a3e2"


# def validate_phone_number(key, phone):  # Verifies that the phone number is valid
#     url = "http://apilayer.net/api/validate?access_key=" + key + "&number=" + phone + "&format=1"  # API request URL
#     verification_request = requests.get(url)  # Make request with API
#
#     verification_response = verification_request.json()  # Capture API response
#     with open('phone_validation.json', 'w') as f:  # Create json file
#         json.dump(verification_response, f)  # Dumps API response into a json file
#
#     valid_phone = verification_response['valid']
#     print("Validating phone number...")
#     print(verification_response)
#     print("Valid = " + str(valid_phone))
#
#     return valid_phone  # Returns phone validation bool


def send_message(key, phone, loan, message):  # Sends message to contact
    url = "https://api-landlinetexting.att.com/message/send?" + "session=" + key + "&contacts=" + phone +\
          "&body=" + message  # API request URL
    send_message_request = requests.get(url)  # Make request with AT&T API
    sleep(.5)

    print("Sending message to " + phone)

    message_response = send_message_request.json()  # Capture API response into json file
    message_status = message_response['success']
    print(message_response)

    status = 'Message Sent'
    description = 'Successfully sent text message to ' + phone

    if not message_status:  # Validates if message was successfully sent
        status = 'Message Not Sent'
        description = message_response['response']['desc']

    data_to_insert = {'Loan Number': loan, 'Phone Number': phone, 'Status': status,
                      'Description': description, 'Date Sent': date_sent}

    writer.writerow(data_to_insert)

    print("--------------------------------------")


for row in read_csv:
    # contact_name = row[0]  # Name is not required in the message
    loan_number = row[0]
    phone_number = row[1]
    due_date = row[2]
    date_sent = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
    body = "Su prestamo que termina en " + loan_number + " vence en " + due_date + "."
    send_message(landline_key, phone_number, loan_number, body)


    # validate_phone_number(numverify_key, phone_number)
    # if validate_phone_number:
    #     send_message(landline_key, phone_number, loan_number, body)
    # else:
    #     print("Phone not valid")


csv_file.close()
report_file.close()
