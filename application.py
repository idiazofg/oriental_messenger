import csv
import io
from ftplib import FTP
from datetime import datetime
from time import sleep
import requests
from flask import Flask, request, render_template, flash, redirect
from flask_mail import Mail
from flask_mail import Message
from flask_sqlalchemy import SQLAlchemy
import os.path
from os import path

app = Flask(__name__, static_url_path='/src/static', static_folder='./src/static')

app.config['DEBUG'] = True
app.config['TESTING'] = False
app.config['MAIL_SERVER'] = 'mail.oriental.local'
app.config['MAIL_PORT'] = 25
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = False
app.config['MAIL_DEBUG'] = True
app.config['MAIL_USERNAME'] = None
app.config['MAIL_PASSWORD'] = None
app.config['MAIL_DEFAULT_SENDER'] = None
app.config['MAIL_MAX_EMAILS'] = None
app.config['MAIL_SUPPRESS_SEND'] = False
app.config['MAIL_ASCII_ATTACHMENTS'] = False

mail = Mail(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)


class Report(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    loan_number = db.Column(db.String(80), unique=True, nullable=False)
    due_date = db.Column(db.String(80), unique=True, nullable=False)
    status = db.Column(db.Boolean(), nullable=False)
    phone_number = db.Column(db.String(20), unique=True, nullable=False)
    date_sent = db.Column(db.DateTime(), nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username


# API Keys
# landline_key = "7eff63c6-cdcf-4907-afc1-40ab9b0aadec:388640903"  # Testing (787-620-0000)
landline_key = "ecbc90e9-3733-4873-a0ff-865e2c982b24:392630102"  # Production (787-777-7117)

# AT&T API URL
att_url = "https://api-landlinetexting.att.com/message/send"


@app.route('/start', methods=["POST"])
def run_script():
    file = request.files['file']

    if not file:
        return 'No file uploaded'

    stream = io.StringIO(file.stream.read().decode("UTF8"), newline=None)
    read_csv = csv.reader(stream)
    next(read_csv)

    report_file = open('D:/home/site/wwwroot/src/api_data/report.csv', mode='w')
    # report_file = open('C:\\Users\\o004726\\IdeaProjects\\oriental_messenger\\src\\api_data\\report.csv', mode='w', newline="")
    fieldnames = ['Loan Number', 'Phone Number', 'Status', 'Description', 'Date Sent']
    writer = csv.DictWriter(report_file, fieldnames=fieldnames)
    writer.writeheader()

    for row in read_csv:
        loan_number = row[0]
        due_date = row[1]
        phone_number = row[2]
        body = "Recordatorio, tu pr%C3%A9stamo que termina en " + loan_number + " vence en " + due_date + "."

        send_message(landline_key, phone_number, loan_number, body, writer)

    report_file.close()

    # msg = Message("Oriental Texting Service Report",
    #               sender="irvin.diaz@orientalbank.com",
    #               recipients=["irvin.diaz@orientalbank.com"])
    #
    # msg.body = 'This is a test email, sent from the Oriental Texting API'
    #
    # with app.open_resource('C:\\Users\\o004726\\IdeaProjects\\oriental_messenger\\src\\api_data\\report.csv') as report:
    #     msg.attach('report.csv', 'text/csv', report.read())
    #
    # mail.send(msg)

    return 'sending message...'


def send_message(key, phone, loan, message, writer):  # Sends message to contact
    payload = "session=" + key + "&contacts=" + phone + "&body=" + message + "&undefined="
    headers = {
        'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8",
        'Cache-Control': "no-cache",
        'Accept': "*/*",
        'Accept-Encoding': "application/gzip",
        'cache-control': "no-cache",
        'Postman-Token': "702187de-a47d-4ce9-bf80-ca1d8a48bfb0"
    }

    response = requests.request("POST", att_url, data=payload, headers=headers)
    sleep(.2)

    print("Sending message to " + phone)

    message_response = response.json()  # Capture API response into json file
    message_status = message_response['success']
    print(message_response)

    status = 'Message Sent'
    description = 'Successfully sent text message to ' + phone
    date_sent = datetime.now().strftime('%d/%m/%Y %H:%M:%S')

    if not message_status:  # Validates if message was successfully sent
        status = 'Message Not Sent'
        description = message_response['response']['desc']

    data_to_insert = {'Loan Number': loan, 'Phone Number': phone, 'Status': status,
                      'Description': description, 'Date Sent': date_sent}

    writer.writerow(data_to_insert)


@app.route('/msg_receive', methods=["POST"])
def receive_event():
    # exists = os.path.isfile('C:\\Users\\o004726\\IdeaProjects\\oriental_messenger\\src\\api_data\\message_log.csv')
    exists = os.path.isfile('D:/home/site/wwwroot/src/api_data/message_log.csv')
    fieldnames_messages = ['Phone Number', 'Message', 'Received at']

    payload = request.json
    message = payload["body"]
    # hasAttachment = payload["hasAttachment"]
    # statusCode = payload["statusCode"]
    phone_number = payload["address"]
    date_created = payload["dateCreated"]
    date_received = datetime.now().strftime('%d/%m/%Y %H:%M:%S %p')

    if exists:
        message_log = open('D:/home/site/wwwroot/src/api_data/message_log.csv', mode='a', newline="")
        # message_log = open('C:\\Users\\o004726\\IdeaProjects\\oriental_messenger\\src\\api_data\\message_log.csv',
        #                    mode='a', newline="")
        writer = csv.DictWriter(message_log, fieldnames=fieldnames_messages)

        data_to_insert = {'Phone Number': phone_number, 'Message': message, 'Received at': date_received}
        writer.writerow(data_to_insert)
        message_log.close()
    else:
        message_log = open('D:/home/site/wwwroot/src/api_data/message_log.csv', mode='w', newline="")
        # message_log = open('C:\\Users\\o004726\\IdeaProjects\\oriental_messenger\\src\\api_data\\message_log.csv',
        #                    mode='w', newline="")
        writer = csv.DictWriter(message_log, fieldnames=fieldnames_messages)
        writer.writeheader()
        # message_log = open('D:/home/site/wwwroot/src/api_data/message_log.csv', mode='a', newline="")
        # message_log = open('C:\\Users\\o004726\\IdeaProjects\\oriental_messenger\\src\\api_data\\message_log.csv',
        #                    mode='a', newline="")
        data_to_insert = {'Phone Number': phone_number, 'Message': message, 'Received at': date_received}
        writer.writerow(data_to_insert)
        message_log.close()

    return "Text received"


@app.route('/email_test')
def send_email():
    msg = Message("Hello, testing flask mail",
                  sender="irvin.diaz@orientalbank.com",
                  recipients=["irvin.diaz@orientalbank.com"])

    msg.body = 'This is a test email, sent from the Oriental Texting API'
    mail.send(msg)

    return 'Email has been sent!'


@app.route('/chartest', methods=["POST"])
def special_char():
    landline_key = "ecbc90e9-3733-4873-a0ff-865e2c982b24:392630102"
    phone = "7875182034"
    message = u"pr\u00E9stamo"
    # message = 'préstamo'

    url = "https://api-landlinetexting.att.com/message/send?" + "session=" + landline_key + "&contacts=" + phone + \
          "&body=" + message  # API request URL

    send_message_request = requests.get(url.encode('utf-8'))  # Make request with AT&T API

    message_response = send_message_request.json()  # Capture API response into json file
    message_status = message_response['success']
    print("Sending message to " + phone)
    print(message_response)

    return "message sent"


@app.route('/msg_read', methods=["POST"])
def read_event():
    payload = request.json
    date_read = payload["dateRead"]
    read = payload["read"]

    fieldnames_reads = ['Date Read', 'Read?']
    reading_log = open('C:\\Users\\o004726\\IdeaProjects\\oriental_messenger\\src\\api_data\\reading_log.csv',
                       mode='a', newline="")
    writer = csv.DictWriter(reading_log, fieldnames=fieldnames_reads)

    data_to_insert = {'Date Read': date_read, 'Read?': read}

    writer.writerow(data_to_insert)
    reading_log.close()

    return "Reading texts..."


@app.errorhandler(500)
def handle_internal_error(e):
    landline_key = "ecbc90e9-3733-4873-a0ff-865e2c982b24:392630102"
    message = "Oriental Text API: 500 INTERNAL SYSTEM ERROR \n" + str(e)

    # API request URL
    url = "https://api-landlinetexting.att.com/message/send?" + "session=" + landline_key + "&contacts=7875182034" + \
          "&body=" + message

    # Make request with AT&T API
    requests.get(url)


@app.route('/')
def main_page():

    return "Oriental SMS Service"
    # return render_template("main/index.html")


if __name__ == '__main__':
    app.run(debug=True)
